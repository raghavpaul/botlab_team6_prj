#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include <stdint.h>
#include <math.h>
#include <pthread.h>
#include <inttypes.h>
#include <unistd.h>

#include "common/getopt.h"
#include "math/gsl_util_matrix.h"
#include "math/gsl_util_blas.h"
#include "math/math_util.h"

#include "lcmtypes/maebot_motor_feedback_t.h"
#include "lcmtypes/maebot_sensor_data_t.h"
#include "lcmtypes/pose_xyt_t.h"

#include "xyt.h"

#define ALPHA_STRING          "0.15"  // longitudinal covariance scaling factor
#define BETA_STRING           "0.15"  // lateral side-slip covariance scaling factor
#define GYRO_RMS_STRING       "0.1"    // [deg/s]

typedef struct state state_t;

struct state {
    getopt_t *gopt;

    lcm_t *lcm;
    const char *odometry_channel;
    const char *feedback_channel;
    const char *sensor_channel;

    // odometry params
    double meters_per_tick; // conversion factor that translates encoder pulses into linear wheel displacement
    double alpha;
    double beta;
    double gyro_rms;

    bool use_gyro;
    int64_t dtheta_utime;
    double dtheta;
    double itheta;
    int64_t itheta_utime;
    double dtheta_sigma;

    double xyt[3]; // 3-dof pose
    double Sigma[3*3];

    int start_trajectory;

    double total_dl, total_dr;

    int baseline_l, baseline_r;

    pthread_mutex_t mutex;

};

static void
motor_feedback_handler (const lcm_recv_buf_t *rbuf, const char *channel,
                        const maebot_motor_feedback_t *msg, void *user)
{
    
  state_t *state = user;

  // Set pose and Sigma to zero when things start
  if (state->start_trajectory == 0){
	  state->xyt[0] = 0.0;
	  state->xyt[1] = 0.0;
	  state->xyt[2] = 0.0;
	
  	  int k;
	  for (k = 0; k < 9; k++){
		  state->Sigma[k] = 0.0;
		}
//Base Line was incorrectly assumed to mean the initial encoder ticks. In the rest of the code, this definiton is carried through 
	  state->baseline_l = msg->encoder_left_ticks;
  	  state->baseline_r = msg->encoder_right_ticks;

	  state->start_trajectory = 1;
	}

    double b = 0.08; // Distance between wheels
    
    double J_plus[3*6];
    
    //Delta 
    double d_l = (msg->encoder_left_ticks - state->baseline_l)*state->meters_per_tick;
    double d_r = (msg->encoder_right_ticks - state->baseline_r)*state->meters_per_tick;
    double d_s = 0.0;

    printf("d_l %f d_r %f \n", d_l, d_r);
	
    state->total_dl += d_l;
    state->total_dr += d_r;
    double delta[3];
    double sigma_t;
    delta[0] = 0.5*d_l+0.5*d_r;
    delta[1] = d_s;
    if(!state->use_gyro)    delta[2] = (1.0/b)*(d_r - d_l);
    else{
      pthread_mutex_lock(&(state->mutex));
      delta[2] = state->dtheta*3.14159/180.0; // From degrees to Radians!
      sigma_t = pow((0.1*(state->dtheta_utime -state->itheta_utime))/1e6,2.0);
      printf("sigma_t = %f\n",sigma_t);
      state->itheta_utime = state->dtheta_utime;
      state->dtheta = 0.0;
      pthread_mutex_unlock(&(state->mutex));
    }
      
    //p_prime
    double p_prime[3];
    xyt_head2tail(p_prime, J_plus, state->xyt, delta);
    memcpy(state->xyt, p_prime, sizeof(p_prime));

//    printf("Next pose %f %f %f \n", p_prime[0], p_prime[1], p_prime[2]);

    // Updates baseline pose  
	state->baseline_l = msg->encoder_left_ticks;
 	state->baseline_r = msg->encoder_right_ticks;
  
   // Sigma
	double sigma_l, sigma_r, sigma_s;
	sigma_l = (state->alpha)*fabs(d_l);
	sigma_r = (state->alpha)*fabs(d_r);
	sigma_s = (state->beta)*fabs(d_l+d_r);

	printf("\n");

	double E_delta[3*3];
  if(!state->use_gyro){
	  E_delta[0] = 0.25*(sigma_l + sigma_r); E_delta[1] = 0 ; E_delta[2] = (0.5/b)*(sigma_r-sigma_l);
	  E_delta[3] = 0; E_delta[4] = sigma_s ; E_delta[5] = 0;
	  E_delta[6] = (0.5/b)*(sigma_r - sigma_l); E_delta[7] = 0 ; E_delta[8] = (1.0/pow(b,2))*(sigma_l+sigma_r);
  }
  else{
	  E_delta[0] = 0.25*(sigma_l + sigma_r); E_delta[1] = 0 ; E_delta[2] = 0.0;
	  E_delta[3] = 0; E_delta[4] = sigma_s ; E_delta[5] = 0;
	  E_delta[6] = 0.0; E_delta[7] = 0 ; E_delta[8] = sigma_t;
  }  

	double E_p[3*3];
	int r ;
	for (r=0;r<3*3;r++){
		E_p[r] = state->Sigma[r];
		}
		
	double Big_Sigma[6*6];

	for (r=0;r<6*6;r++){
		Big_Sigma[r] = 0.0;
		}
 
	double Intermediate[3*6];

	for (r=0;r<3*6;r++){
		Intermediate[r] = 0.0;
		}

	double Next_Sigma[3*3];

	for ( r = 0 ; r < 3*3 ; r++){
		Next_Sigma[r] = 0.0;
		}

	Big_Sigma[0] = E_p[0]; Big_Sigma[1] = E_p[1]; Big_Sigma[2] = E_p[2];
	Big_Sigma[6] = E_p[3]; Big_Sigma[7] = E_p[4]; Big_Sigma[8] = E_p[5];
	Big_Sigma[12] = E_p[6]; Big_Sigma[13] = E_p[7]; Big_Sigma[14] = E_p[8];

	Big_Sigma[21] = E_delta[0]; Big_Sigma[22] = E_delta[1]; Big_Sigma[23] = E_delta[2];
	Big_Sigma[27] = E_delta[3]; Big_Sigma[28] = E_delta[4]; Big_Sigma[29] = E_delta[5];
	Big_Sigma[33] = E_delta[6]; Big_Sigma[34] = E_delta[7]; Big_Sigma[35] = E_delta[8];

	//J_plus*Big_Sigma*J_plusT;

	gsl_matrix_view J_plus_gsl = gsl_matrix_view_array(J_plus, 3, 6);
	gsl_matrix_view Big_Sigma_gsl  = gsl_matrix_view_array(Big_Sigma, 6, 6);
	gsl_matrix_view Intermediate_gsl  = gsl_matrix_view_array(Intermediate, 3, 6);
	gsl_matrix_view Next_Sigma_gsl  = gsl_matrix_view_array(Next_Sigma, 3, 3);

	gsl_blas_dgemm(CblasNoTrans, CblasNoTrans, 1.0, &J_plus_gsl.matrix, &Big_Sigma_gsl.matrix, 0.0, &Intermediate_gsl.matrix);
	gsl_blas_dgemm(CblasNoTrans, CblasTrans, 1.0, &Intermediate_gsl.matrix, &J_plus_gsl.matrix, 0.0, &Next_Sigma_gsl.matrix);


	//memcpy(state->Sigma, &Next_Sigma_gsl.matrix, 9*sizeof(double));
	int w,z;
	for ( w = 0; w < 3; w++){
		for ( z = 0; z < 3 ;z++){
			state->Sigma[3*w+z] = gsl_matrix_get(&Next_Sigma_gsl.matrix,w,z);
		}
	}
	printf("Next Sigma %f %f %f %f\n", state->Sigma[0],  state->Sigma[1], state->Sigma[3],  state->Sigma[4]);

	printf(" \n ");
	printf("total_dl = %f %f", state->total_dl, state->total_dr);

    
    // publish
    pose_xyt_t odo = { .utime = msg->utime };
    memcpy (odo.xyt, state->xyt, sizeof state->xyt);
    memcpy (odo.Sigma, state->Sigma, sizeof state->Sigma);
    pose_xyt_t_publish (state->lcm, state->odometry_channel, &odo);

}

static void
sensor_data_handler (const lcm_recv_buf_t *rbuf, const char *channel,
                     const maebot_sensor_data_t *msg, void *user)
{
    state_t *state = user;

    static int64_t initial_theta;
    static int64_t initial_time;
    static int64_t theta;
    static int64_t theta_time;
    static double bias;
    static int npoints;
    static int flag;

    if (!state->use_gyro)
        return;
    
    // Initial Time and Value
    if(!initial_time){
      initial_time = msg->utime;
      initial_theta = msg->gyro_int[2];
      return;
    }

    // Initial Calibration
    if((!flag) &&(msg->utime > (initial_time + 10000000))){
	    bias = msg->gyro_int[2] - initial_theta;
            bias = bias / (msg->utime - initial_time);
	    flag = 1;
	    theta = msg->gyro_int[2];
	    theta_time = msg->utime;
	  }	
    // Bias Compensated Value
    else if (flag){
	    pthread_mutex_lock(&(state->mutex));
	    if(!state->dtheta){
		    initial_theta = theta;
		    initial_time = theta_time;
        state->itheta_utime = initial_time;
		    flag = 1;
	    }
 	    theta = msg->gyro_int[2];
    	theta_time = msg->utime;
   
      state->dtheta = theta - initial_theta - bias*(theta_time - initial_time);
      state->dtheta = state->dtheta/131e6;
	    printf("initial_theta = %"PRId64" \t theta = %"PRId64" \t dtheta = %f\n", initial_theta, theta, state->dtheta); 
	    printf("initial_time = %"PRId64" \t theta_time = %"PRId64" \n", initial_time, theta_time); 
      pthread_mutex_unlock(&(state->mutex));
    }

}

//TEST PROGRAMS
void test_inverse(){
    double X_ij[3] ={4, 5, 1.57};
    double X_ji[3] ={0};
    xyt_inverse(X_ji, NULL ,X_ij);

    printf("inverse() tests X_ji %f %f %f \n", X_ji[0], X_ji[1], X_ji[2]);
	}
	
void test_head2tail(){
	double X_ij[3] = {2.0, 3.0, 1.57};
	double X_jk[3] = {1.0, 0.0, -1.57};
	double X_ik[3] = {0};
	
	xyt_head2tail(X_ik, NULL, X_ij, X_jk);
	
	printf("head2tail() test X_ik %f %f %f", X_ik[0], X_ik[1], X_ik[2]);
	} 

void test_tail2tail(){
	double X_ij[3] = {1.0, 3.0, 1.57};
	double X_jk[3] = {0};
	double X_ik[3] = {2.0,4.0,-1.57};
	
	xyt_tail2tail(X_jk, NULL, X_ij, X_ik);
	
	printf("tail2tail() test X_jk %f %f %f", X_jk[0], X_jk[1], X_jk[2]);
	}

int
main (int argc, char *argv[])
{

    
    // so that redirected stdout won't be insanely buffered.
    setvbuf (stdout, (char *) NULL, _IONBF, 0);

    state_t *state = calloc (1, sizeof *state);

    state->start_trajectory = 0;

    state->total_dl = 0.0 ; state->total_dr = 0.0; 
    state->meters_per_tick = (1.0/4774.6);

    state->gopt = getopt_create ();
    getopt_add_bool   (state->gopt, 'h', "help", 0, "Show help");
    getopt_add_bool   (state->gopt, 'g', "use-gyro", 0, "Use gyro for heading instead of wheel encoders");
    getopt_add_string (state->gopt, '\0', "odometry-channel", "BOTLAB_ODOMETRY", "LCM channel name");
    getopt_add_string (state->gopt, '\0', "feedback-channel", "MAEBOT_MOTOR_FEEDBACK", "LCM channel name");
    getopt_add_string (state->gopt, '\0', "sensor-channel", "MAEBOT_SENSOR_DATA", "LCM channel name");
    getopt_add_double (state->gopt, '\0', "alpha", ALPHA_STRING, "Longitudinal covariance scaling factor");
    getopt_add_double (state->gopt, '\0', "beta", BETA_STRING, "Lateral side-slip covariance scaling factor");
    getopt_add_double (state->gopt, '\0', "gyro-rms", GYRO_RMS_STRING, "Gyro RMS deg/s");

    if (!getopt_parse (state->gopt, argc, argv, 1) || getopt_get_bool (state->gopt, "help")) {
        printf ("Usage: %s [--url=CAMERAURL] [other options]\n\n", argv[0]);
        getopt_do_usage (state->gopt);
        exit (EXIT_FAILURE);
    }

    state->use_gyro = getopt_get_bool (state->gopt, "use-gyro");
    state->odometry_channel = getopt_get_string (state->gopt, "odometry-channel");
    state->feedback_channel = getopt_get_string (state->gopt, "feedback-channel");
    state->sensor_channel = getopt_get_string (state->gopt, "sensor-channel");
    state->alpha = getopt_get_double (state->gopt, "alpha");
    state->beta = getopt_get_double (state->gopt, "beta");
    state->gyro_rms = getopt_get_double (state->gopt, "gyro-rms") * DTOR;

    // initialize LCM
    state->lcm = lcm_create (NULL);
    maebot_sensor_data_t_subscribe (state->lcm, state->sensor_channel,
                                    sensor_data_handler, state);
    sleep(11);
    maebot_motor_feedback_t_subscribe (state->lcm, state->feedback_channel,
                                       motor_feedback_handler, state);
    printf ("ticks per meter: %f\n", 1.0/state->meters_per_tick);
    test_head2tail();
    test_tail2tail();
    test_inverse();
    while (1)
        lcm_handle (state->lcm);
	
	
    	
}

