#include <stdio.h>
#include <stdlib.h>
#include <inttypes.h>

#include <lcm/lcm.h>
#include "lcmtypes/maebot_sensor_data_t.h"


static void
sensor_data_handler (const lcm_recv_buf_t *rbuf, const char *channel,
                     const maebot_sensor_data_t *msg, void *user)
{
    static FILE* output;  
    static int64_t time;
    static int64_t gyroint0, gyroint1, gyroint2;

    int res = system ("clear");
    if (res)
        printf ("system clear failed\n");

    if (!output){
      output = fopen("gyros.dat","w");
      fprintf(output,"#time, gyro[0], gyro[1], gyro[2], gyro_int[0], gyro_int[1], gyro_int[2]\n");
      time = msg->utime;
	gyroint0 = msg->gyro_int[0];
	gyroint1 = msg->gyro_int[1];
	gyroint2 = msg->gyro_int[2];
    }

    fprintf(output,"%"PRId64" %d %d %d %"PRId64" %"PRId64" %"PRId64"\n",msg->utime - time,msg->gyro[0],msg->gyro[1],msg->gyro[2],msg->gyro_int[0]-gyroint0,msg->gyro_int[1]-gyroint1,msg->gyro_int[2]-gyroint2);

/*
    printf ("Subscribed to channel: MAEBOT_SENSOR_DATA\n");
    printf ("utime: %"PRId64"\n", msg->utime);
    printf ("accel[0, 1, 2]:        %d,\t%d,\t%d\n",
            msg->accel[0], msg->accel[1], msg->accel[2]);
    printf ("gyro[0, 1, 2]:         %d,\t%d,\t%d\n",
            msg->gyro[0], msg->gyro[1], msg->gyro[2]);
    printf ("gyro_int[0, 1, 2]:     %"PRId64",\t%"PRId64",\t%"PRId64"\n",
            msg->gyro_int[0], msg->gyro_int[1], msg->gyro_int[2]);
    printf ("line_sensors[0, 1, 2]: %d,\t%d,\t%d\n",
            msg->line_sensors[0], msg->line_sensors[1], msg->line_sensors[2]);
    printf ("range: %d\n", msg->range);
    printf ("user_button_pressed: %s\n", msg->user_button_pressed ? "true" : "false");
    printf ("power_button_pressed: %s\n", msg->power_button_pressed ? "true" : "false");
*/
}

int
main (int argc, char *argv[])
{
    // so that redirected stdout won't be insanely buffered.
    setvbuf (stdout, (char *) NULL, _IONBF, 0);

    lcm_t *lcm = lcm_create (NULL);
    if (!lcm)
        return EXIT_FAILURE;

    maebot_sensor_data_t_subscribe (lcm,
                                    "MAEBOT_SENSOR_DATA",
                                    sensor_data_handler,
                                    NULL);

    while (1)
        lcm_handle (lcm);

    return EXIT_SUCCESS;

}
